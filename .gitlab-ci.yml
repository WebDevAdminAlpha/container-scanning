---
include:
  - template: Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml

# Workaround to the detached pipeline as described in
# https://gitlab.com/gitlab-org/gitlab/-/issues/34756
workflow:
  rules:
    - if: '$CI_MERGE_REQUEST_EVENT_TYPE == "detached"'
      when: never
    - when: always

# When using dind, it's wise to use the overlayfs driver for
# improved performance.
variables:
  # The context of these variables is global to the downstream job
  DOCKER_DRIVER: overlay2
  MAJOR: 4
  TMP_IMAGE: $CI_REGISTRY_IMAGE/tmp/$SCANNER:$CI_COMMIT_SHA
  SCANNER: trivy

stages:
  # check, test, and scan the Docker images
  - initial-test
  - build-image
  - integration-test
  # release Docker images and distro packages
  - release-version
  # update Docker images and distro packages of the major release
  - release-major
  # for scheduled pipeline, we release same image everyday to keep
  # vulnerability db updated
  - maintenance

.not-on-schedule:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_PIPELINE_SOURCE != "schedule"'
      when: on_success

.ruby-alpine:
  extends: .not-on-schedule
  image: ruby:2.7.2-alpine
  before_script:
    - apk add git
    - bundle config --local jobs "$(nproc)"
    - bundle config set no-cache 'true'
    - bundle install --quiet

# Yamllint of CI-related yaml and changelogs.
lint-yaml:
  extends: .not-on-schedule
  image: pipelinecomponents/yamllint:latest
  stage: initial-test
  variables:
    LINT_PATHS: .gitlab-ci.yml .rubocop.yml spec/fixtures
  script:
    - yamllint -c .yamllint -f colored $LINT_PATHS

shellcheck:
  extends: .not-on-schedule
  stage: initial-test
  image: koalaman/shellcheck-alpine:stable
  script:
    - shellcheck script/*

check-commit-message:
  extends: .ruby-alpine
  stage: initial-test
  script:
    - bundle exec rake commit_message
  allow_failure: true

unit test:
  extends: .ruby-alpine
  stage: initial-test
  script:
    - bundle exec rake unit_test

gitlab styles:
  extends: .ruby-alpine
  stage: initial-test
  script:
    - bundle exec rubocop

dependency_scanning:
  stage: initial-test

gemnasium-dependency_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes:
        - Gemfile.lock

bundler-audit-dependency_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes:
        - Gemfile.lock

license_scanning:
  stage: initial-test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - changes:
        - Gemfile.lock

.grype_scanner:
  allow_failure: true
  variables:
    SCANNER: grype

.build-tmp-image:
  extends: .not-on-schedule
  image: docker:stable
  stage: build-image
  services:
    - docker:19.03.5-dind
  script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - docker build --build-arg SCANNER -q -t $TMP_IMAGE .
    - docker push $TMP_IMAGE

build-trivy-image:
  extends: .build-tmp-image

build-grype-image:
  extends:
    - .build-tmp-image
    - .grype_scanner

.integration_test:
  extends: .not-on-schedule
  image: $TMP_IMAGE
  stage: integration-test
  variables:
    IMAGE_TAG: $TMP_IMAGE
  script:
    - sudo ./script/setup_integration
    - bundle exec rake integration

.alpine_test:
  extends: .integration_test
  script:
    - sudo ./script/setup_integration
    - bundle exec rake spec_integration_alpine

.centos_test:
  extends: .integration_test
  script:
    - sudo ./script/setup_integration
    - bundle exec rake spec_integration_centos

.webgoat_test:
  extends: .integration_test
  script:
    - sudo ./script/setup_integration
    - bundle exec rake spec_integration_webgoat

.ca_cert_test:
  extends: .integration_test
  script:
    - sudo ./script/setup_integration
    - bundle exec rake spec_integration_ca_cert

alpine trivy:
  extends: .alpine_test

alpine grype:
  extends:
    - .alpine_test
    - .grype_scanner

centos trivy:
  extends: .centos_test

centos grype:
  extends:
    - .centos_test
    - .grype_scanner

webgoat trivy:
  extends: .webgoat_test

webgoat grype:
  extends:
    - .webgoat_test
    - .grype_scanner

ca cert trivy:
  extends: .ca_cert_test

ca cert grype:
  extends:
    - .ca_cert_test
    - .grype_scanner

.docker_tag:
  extends: .not-on-schedule
  image: docker:stable
  stage: release-version
  services:
    - docker:19.03.5-dind
  script:
    - docker info
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
    - export SOURCE_IMAGE=$TMP_IMAGE
    - export TARGET_IMAGE=$CI_REGISTRY_IMAGE:${IMAGE_TAG:-$CI_JOB_NAME}
    - docker pull $SOURCE_IMAGE
    - docker tag $SOURCE_IMAGE $TARGET_IMAGE
    - docker push $TARGET_IMAGE

tag branch:
  extends: .docker_tag
  variables:
    # CAUTION: by preferring `SLUG` over `NAME` we can properly handle
    # non-alphanumeric characters, but this may limit our tags to 63 chars
    # or raise potential conflicts.
    IMAGE_TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME
      when: never
    - if: $CI_COMMIT_BRANCH

tag edge:
  extends: .docker_tag
  variables:
    IMAGE_TAG: edge
  rules:
    - if: $CI_DEFAULT_BRANCH == $CI_COMMIT_REF_NAME

tag version:
  extends: .docker_tag
  before_script:
    - export IMAGE_TAG=${CI_COMMIT_TAG/v/}
    - bundle exec rake check_version
  rules:
    - if: $CI_COMMIT_TAG

.release:
  extends: .docker_tag
  stage: release-major
  rules:
    - if: $CI_COMMIT_TAG

major:
  extends: .release
  variables:
    IMAGE_TAG: $MAJOR

update_changelog:
  extends: .ruby-alpine
  stage: maintenance
  script:
    - bundle exec rake changelog
  rules:
    # consider pipelines triggered via API to be coming from trigger-db-update; they would have
    # already been run on the original pipeline when the tag first appeared. Also ignore schedule
    # pipelines.
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
      when: never
    - if: '$CI_PIPELINE_SOURCE == "api"'
      when: never
    - if: $CI_COMMIT_TAG

update_trivy_binary:
  extends: .ruby-alpine
  stage: maintenance
  script: bundle exec rake update_trivy
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'

trigger-db-update:
  extends: .ruby-alpine
  stage: maintenance
  script: bundle exec rake trigger_db_update
  rules:
    - if: '$TRIGGER_DB_UPDATE != "" && $CI_PIPELINE_SOURCE == "schedule"'

latest:
  extends: .release
