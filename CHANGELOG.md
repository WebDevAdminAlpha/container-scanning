## 4.1.7 (2021-05-13)

No changes.

## 4.1.6 (2021-05-11)

### added (2 changes)

- [Update rubocop rules](gitlab-org/security-products/analyzers/container-scanning@3604b92decec94da9db20665b6a494704c626f33) ([merge request](gitlab-org/security-products/analyzers/container-scanning!26))
- [Add maintenance job to keep vulnerability db updated](gitlab-org/security-products/analyzers/container-scanning@13176a521b7276878fadb415965c05a4f7680c9f) ([merge request](gitlab-org/security-products/analyzers/container-scanning!20))

### Added (2 changes)

- [Add job for checking commit message format](gitlab-org/security-products/analyzers/container-scanning@f7e86499f08493dbd5d8979458190cd4a605c940) ([merge request](gitlab-org/security-products/analyzers/container-scanning!23))
- [Use gitlab changelog generator](gitlab-org/security-products/analyzers/container-scanning@a160206edd85a3fad1460cdffc1e8e1fdcda2ecc) ([merge request](gitlab-org/security-products/analyzers/container-scanning!22))
